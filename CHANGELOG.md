# Changelog

## 1.1

- Switch to new array representation

## 1.0

- Initial import from [svn](https://svn.cs.ru.nl/repos/clean-tools/)
- Switch to nitrile
