#ifdef _WIN32
#include <windows.h>
#else
#include <stdlib.h>
#endif

#include "Clean.h"
#include "example_int4.h"

void *my_malloc (int size_in_bytes)
{
#ifdef _WIN32
	return GlobalAlloc (0,size_in_bytes);
#else
	return malloc(size_in_bytes);
#endif
}

int my_free (void *p)
{
#ifdef _WIN32
	return GlobalFree(p)!=NULL;
#else
	free(p);
	return 0;
#endif
}

void store_int (int *p,int offset,int value)
{
	p[offset]=value;
}

int add_p (int *p1,int *p2)
{
	return *p1 + *p2;
}

