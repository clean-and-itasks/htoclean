#!/bin/sh
set -ex
#for f in *.h
for f in example_const1 example_enum1 example_int1 example_int2 example_int3 example_int4 example_string1 example_tuple1 example_type1 example_array1
do
	htoclean "$f"
done
mkdir -p "Clean System Files"
for f in example_const1_c.c example_enum1_c.c example_int1_c.c example_int2_c.c example_int3_c.c example_int4_c.c example_string1_c.c example_tuple1_c.c example_array1_c.c
do
	"${CC:-gcc}" ${CFLAGS} -I../include -c "$f" -o "Clean System Files/$(basename -s ".c" "$f").o"
done
