# htoclean

Tool to convert C header files to Clean definitions.
Also includes a Clean header file for interacting with Clean datatypes from C.

## Maintainer

Mart Lubbers (mart@cs.ru.nl)

## License

`htoclean` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).
